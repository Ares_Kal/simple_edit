using System;
using System.Windows.Forms;
using System.Drawing;


namespace DrawEngine.Tools
{
	/// <summary>
	/// Abstract base class for all shapes.
	/// </summary>
	abstract class ToolBase
	{

        /// <summary>
        /// Left mouse button is pressed.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public virtual void OnMouseDown(DrawSurface drawArea, MouseEventArgs e)
        {
        }


        /// <summary>
        /// Mouse is moved, left mouse button is pressed or no button is pressed.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public virtual void OnMouseMove(DrawSurface drawArea, MouseEventArgs e)
        {
        }


        /// <summary>
        /// Left mouse button is released.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public virtual void OnMouseUp(DrawSurface drawArea, MouseEventArgs e)
        {
        }

        /// <summary>
        /// Returns a Point object representing the mouse hit with the applied coordinate transform.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected Point GetMouseHit(DrawSurface drawArea, MouseEventArgs e)
        {
            Point p = new Point(Math.Abs(drawArea.AutoScrollPosition.X) + e.X, Math.Abs(drawArea.AutoScrollPosition.Y) + e.Y);
            return p;
        }
    }
}
