using System;
using System.Windows.Forms;
using System.Drawing;


namespace DrawEngine.Tools
{
	/// <summary>
	/// Rectangle tool
	/// </summary>
	class ToolRectangle : ToolObject
	{

		public ToolRectangle()
		{
            Cursor = new Cursor(GetType(), "Rectangle.cur");
		}

        public override void OnMouseDown(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);

            AddNewObject(drawArea, new DrawRectangle(pointscroll.X, pointscroll.Y, 1, 1));
        }

        public override void OnMouseMove(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);
            drawArea.Cursor = Cursor;

            if ( e.Button == MouseButtons.Left )
            {
                drawArea.GraphicsCollection[0].MoveHandleTo(pointscroll, 5);
                drawArea.Refresh();
                drawArea.GraphicsCollection.Dirty = true;
            }
        }
	}
}
