using System;
using System.Windows.Forms;
using System.Drawing;

namespace DrawEngine.Tools
{
	/// <summary>
	/// Base class for all tools which create new graphic object
	/// </summary>
	abstract class ToolObject : ToolBase
	{
        private Cursor cursor;

        /// <summary>
        /// Tool cursor.
        /// </summary>
        protected Cursor Cursor
        {
            get
            {
                return cursor;
            }
            set
            {
                cursor = value;
            }
        }


        /// <summary>
        /// Left mouse is released.
        /// New object is created and resized.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseUp(DrawSurface drawArea, MouseEventArgs e)
        {
            drawArea.GraphicsCollection[0].Normalize();
            drawArea.AddCommandToHistory(new CmdAdd(drawArea.GraphicsCollection[0]));
            drawArea.ActiveTool = DrawSurface.ToolType.Pointer;

            drawArea.Capture = false;
            drawArea.Refresh();
            drawArea.GraphicsCollection.Dirty = true;
        }

        /// <summary>
        /// Add new object to draw area.
        /// Function is called when user left-clicks draw area,
        /// and one of ToolObject-derived tools is active.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="o"></param>
        protected void AddNewObject(DrawSurface drawArea, DrawShapeBase o)
        {
            drawArea.GraphicsCollection.UnselectAll();

            o.Selected = true;
            drawArea.GraphicsCollection.Add(o);

            drawArea.Capture = true;
            drawArea.Refresh();

        }

        protected void FillObject(DrawSurface drawArea, Point p)
        {
            foreach (var shape in drawArea.GraphicsCollection)
            {
                if (shape.IsWithinBounds(p))
                {
                    shape.IsFilled = true;
                    shape.AreaColor = drawArea.ActiveColor;
                    break;
                }
            }

            drawArea.Capture = true;
            drawArea.Refresh();
        }
	}
}
