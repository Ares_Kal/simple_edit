namespace DrawEngine.Tools
{
    using System;
    using System.Windows.Forms;
    using System.Drawing;
    using DrawEngine;
    
    /// <summary>
	/// Ellipse tool
	/// </summary>
	class ToolEllipse : ToolRectangle
	{
		public ToolEllipse()
		{
            Cursor = new Cursor(GetType(), "Ellipse.cur");
		}

        public override void OnMouseDown(DrawSurface drawArea, MouseEventArgs e)
        {
            Point p = GetMouseHit(drawArea, e);
            AddNewObject(drawArea, new DrawEllipse(p.X, p.Y, 1, 1));
        }

	}
}
