﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace DrawEngine.Tools
{
    /// <summary>
    /// This tool is used to fill the area inside of an object with the chosen color.
    /// </summary>
    class ToolBucket : ToolObject
    {
  
        public ToolBucket()
        {
            Cursor = new Cursor(GetType(), "Bucket.cur");
    
        }

        /// <summary>
        /// Left mouse button is pressed.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseDown(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);
            FillObject(drawArea, pointscroll);
        }

        /// <summary>
        /// Mouse is released.
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseUp(DrawSurface drawArea, MouseEventArgs e)
        {
           
        }

        public override void OnMouseMove(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);
            drawArea.Cursor = Cursor;
        }
    }
}
