﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawEngine.Tools
{
    public class FillEventArgs
    {
        public Color Color { get; set; }
        public Graphics Graphics { get; set; }

        public Point HitPoint { get; set; }
        public FillEventArgs(Graphics graphics, Point point, Color color)
        {
            Color = color;
            Graphics = graphics;
            HitPoint = point;
        }
    }

    public interface IFillAreaObserver
    {
        event EventHandler<FillEventArgs> AreaFill;
    }
}
