using System;
using System.Windows.Forms;
using System.Drawing;


namespace DrawEngine.Tools
{
	/// <summary>
	/// Pointer tool
	/// </summary>
	class ToolPointer : ToolBase
	{
        private enum SelectionMode
        {
            None,
            NetSelection,   // group selection is active
            Move,           // object(s) are moves
            Size            // object is resized
        }

        private SelectionMode selectMode = SelectionMode.None;

        // Object which is currently resized:
        private DrawShapeBase resizedObject;
        private int resizedObjectHandle;

        // Keep state about last and current pointscroll (used to move and resize objects)
        private Point lastPoint = new Point(0,0);
        private Point startPoint = new Point(0, 0);

        private CmdChange commandChangeState;
        bool wasMove;

		public ToolPointer()
		{
		}

        /// <summary>
        /// Left mouse button is pressed
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseDown(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);
            commandChangeState = null;
            wasMove = false;

            selectMode = SelectionMode.None;

            // Test for resizing (only if control is selected, cursor is on the handle)
            foreach (DrawShapeBase o in drawArea.GraphicsCollection.Selection)
            {
                int handleNumber = o.HitTest(pointscroll);

                if (handleNumber > 0)
                {
                    selectMode = SelectionMode.Size;

                    // keep resized object in class member
                    resizedObject = o;
                    resizedObjectHandle = handleNumber;

                    // Since we want to resize only one object, unselect all other objects
                    drawArea.GraphicsCollection.UnselectAll();
                    o.Selected = true;

                    commandChangeState = new CmdChange(drawArea.GraphicsCollection);

                    drawArea.GraphicsCollection.Dirty = true;
                    break;
                }
            }

            // Test for move (cursor is on the object)
            if ( selectMode == SelectionMode.None )
            {
                int n1 = drawArea.GraphicsCollection.Count;
                DrawShapeBase o = null;

                for ( int i = 0; i < n1; i++ )
                {
                    if ( drawArea.GraphicsCollection[i].HitTest(pointscroll) == 0 )
                    {
                        o = drawArea.GraphicsCollection[i];
                        break;
                    }
                }

                if ( o != null )
                {
                    selectMode = SelectionMode.Move;

                    // Unselect all if Ctrl is not pressed and clicked object is not selected yet
                    if ( ( Control.ModifierKeys & Keys.Control ) == 0  && !o.Selected )
                        drawArea.GraphicsCollection.UnselectAll();

                    // Select clicked object
                    o.Selected = true;

                    commandChangeState = new CmdChange(drawArea.GraphicsCollection);

                    drawArea.Cursor = Cursors.SizeAll;
                    drawArea.GraphicsCollection.Dirty = true;
                }
            }

            // Net selection
            if ( selectMode == SelectionMode.None )
            {
                // click on background
                if ( ( Control.ModifierKeys & Keys.Control ) == 0 )
                    drawArea.GraphicsCollection.UnselectAll();

                selectMode = SelectionMode.NetSelection;

            }

            lastPoint.X = e.X;
            lastPoint.Y = e.Y;
            startPoint.X = e.X;
            startPoint.Y = e.Y;

            drawArea.Capture = true;

            drawArea.Refresh();

            if ( selectMode == SelectionMode.NetSelection )
            {
                // Draw selection rectangle in initial position
                ControlPaint.DrawReversibleFrame(
                    drawArea.RectangleToScreen(DrawRectangle.GetNormalizedRectangle(startPoint, lastPoint)),
                    Color.Black,
                    FrameStyle.Dashed);
            }
        }


        /// <summary>
        /// Mouse is moved.
        /// None button is pressed, or left button is pressed.
        /// </summary>
        /// <param name="drawSurface"></param>
        /// <param name="e"></param>
        public override void OnMouseMove(DrawSurface drawSurface, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawSurface, e);
            Point oldPoint = lastPoint;

            wasMove = true;

            // set cursor when mouse button is not pressed
            if (e.Button == MouseButtons.None)
            {
                Cursor cursor = null;

                for (int i = 0; i < drawSurface.GraphicsCollection.Count; i++)
                {
                    int n = drawSurface.GraphicsCollection[i].HitTest(pointscroll);

                    if (n > 0)
                    {
                        cursor = drawSurface.GraphicsCollection[i].GetHandleCursor(n);
                        break;
                    }
                }

                if (cursor == null)
                    cursor = Cursors.Default;

                drawSurface.Cursor = cursor;

                return;
            }

            if (e.Button != MouseButtons.Left)
                return;

            /// Left button is pressed

            // Find difference between previous and current position
            int dx = e.X - lastPoint.X;
            int dy = e.Y - lastPoint.Y;

            lastPoint.X = e.X;
            lastPoint.Y = e.Y;

            // resize
            if (selectMode == SelectionMode.Size)
            {
                if (resizedObject != null)
                {
                    resizedObject.MoveHandleTo(pointscroll, resizedObjectHandle);
                    drawSurface.Refresh();
                    drawSurface.GraphicsCollection.Dirty = true;
                }
            }

            // move
            if (selectMode == SelectionMode.Move)
            {
                foreach (DrawShapeBase o in drawSurface.GraphicsCollection.Selection)
                {
                    o.Move(dx, dy);
                }

                drawSurface.Cursor = Cursors.SizeAll;
                drawSurface.Refresh();
                drawSurface.GraphicsCollection.Dirty = true;
            }

            if (selectMode == SelectionMode.NetSelection)
            {
                // Remove old selection rectangle
                ControlPaint.DrawReversibleFrame(
                    drawSurface.RectangleToScreen(DrawRectangle.GetNormalizedRectangle(startPoint, oldPoint)),
                    Color.Black,
                    FrameStyle.Dashed);

                // Draw new selection rectangle
                ControlPaint.DrawReversibleFrame(
                    drawSurface.RectangleToScreen(DrawRectangle.GetNormalizedRectangle(startPoint, new Point(e.X, e.Y))),
                    Color.Black,
                    FrameStyle.Dashed);

                return;
            }

        }

        /// <summary>
        /// Right mouse button is released
        /// </summary>
        /// <param name="drawArea"></param>
        /// <param name="e"></param>
        public override void OnMouseUp(DrawSurface drawArea, MouseEventArgs e)
        {
            if ( selectMode == SelectionMode.NetSelection )
            {
                // Remove old selection rectangle
                ControlPaint.DrawReversibleFrame(
                    drawArea.RectangleToScreen(DrawRectangle.GetNormalizedRectangle(startPoint, lastPoint)),
                    Color.Black,
                    FrameStyle.Dashed);

                // Make group selection
                drawArea.GraphicsCollection.SelectInRectangle(
                    DrawRectangle.GetNormalizedRectangle(new Point(startPoint.X + Math.Abs(drawArea.AutoScrollPosition.X), startPoint.Y + Math.Abs(drawArea.AutoScrollPosition.Y)),
                                                        new Point(lastPoint.X + Math.Abs(drawArea.AutoScrollPosition.X), lastPoint.Y + Math.Abs(drawArea.AutoScrollPosition.Y))));

                selectMode = SelectionMode.None;
            }

            if ( resizedObject != null )
            {
                // after resizing
                resizedObject.Normalize();
                resizedObject = null;
            }

            drawArea.Capture = false;
            drawArea.Refresh();
            drawArea.GraphicsCollection.Dirty = true;

            if ( commandChangeState != null  && wasMove )
            {
                // Keep state after moving/resizing and add command to history
                commandChangeState.NewState(drawArea.GraphicsCollection);
                drawArea.AddCommandToHistory(commandChangeState);
                commandChangeState = null;
            }
        }
	}
}
