﻿using DrawEngine.Shapes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawEngine.Tools
{
    class ToolTriangle : ToolObject
    {
        public ToolTriangle()
        {
            Cursor = new Cursor(GetType(), "Triangle.cur");
        }
        public override void OnMouseDown(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);
            AddNewObject(drawArea, new DrawTriangle(pointscroll.X, pointscroll.Y, pointscroll.X + 3, pointscroll.Y - 3, pointscroll.X + 6, pointscroll.Y - 3));
        }

        public override void OnMouseMove(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);
            drawArea.Cursor = Cursor;

            if (e.Button == MouseButtons.Left)
            {
                drawArea.GraphicsCollection[0].MoveHandleTo(pointscroll, 2);
                drawArea.Refresh();
                drawArea.GraphicsCollection.Dirty = true;
            }
        }
    }
}
