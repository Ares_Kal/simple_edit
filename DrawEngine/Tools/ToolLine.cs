using System;
using System.Windows.Forms;
using System.Drawing;

namespace DrawEngine.Tools
{
	/// <summary>
	/// Line tool
	/// </summary>
	class ToolLine : ToolObject
	{
        public ToolLine()
        {
            Cursor = new Cursor(GetType(), "Line.cur");
        }

        public override void OnMouseDown(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);
            AddNewObject(drawArea, new DrawLine(pointscroll.X, pointscroll.Y, pointscroll.X + 1, pointscroll.Y + 1));
        }

        public override void OnMouseMove(DrawSurface drawArea, MouseEventArgs e)
        {
            Point pointscroll = GetMouseHit(drawArea, e);
            drawArea.Cursor = Cursor;

            if ( e.Button == MouseButtons.Left )
            {
                drawArea.GraphicsCollection[0].MoveHandleTo(pointscroll, 2);
                drawArea.Refresh();
                drawArea.GraphicsCollection.Dirty = true;
            }
        }
    }
}
