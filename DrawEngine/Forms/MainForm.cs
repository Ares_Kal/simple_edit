using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Security;
using Microsoft.Win32;


namespace DrawEngine
{
    public partial class MainForm : Form
    {
        #region Members
        Dictionary<Type, System.Reflection.MethodInfo> commandRefs;
        #endregion
        delegate void callback(DrawShapeBase obj, Graphics g);
        void InitializeCommandRefs()
        {

            commandRefs = new Dictionary<Type, System.Reflection.MethodInfo>();
            var types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes();
            foreach (var type in types)
            {
                if (type == typeof(DrawShapeBase) || type.IsSubclassOf(typeof(DrawShapeBase)))
                {
                    System.Reflection.MethodInfo method = type.GetMethod("Draw", new Type[] { typeof(Graphics) });
                    commandRefs.Add(type, method);
                }
            }
        }



        #region Properties
        /// <summary>
        /// Get reference to Edit menu item.
        /// Used to show context menu in DrawArea class.
        /// </summary>
        /// <value></value>
        public ToolStripMenuItem ContextParent
        {
            get
            {
                return editToolStripMenuItem;
            }
        }

        #endregion

        #region Ctor
        public MainForm()
        {
            InitializeComponent();
            drawSurface.StatusLabel = this.toolStripStatusLabel;
            //InitializeCommandRefs();

        }

        #endregion

        #region Toolbar Event Handlers

        private void toolStripButtonNew_Click(object sender, EventArgs e)
        {
            FileNew();
        }

        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            FileOpen();
        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            FileSave();
        }

        private void toolStripButtonPointer_Click(object sender, EventArgs e)
        {
            DrawPointer();
        }

        private void toolStripButtonRectangle_Click(object sender, EventArgs e)
        {
            DrawRectangle();
        }

        private void toolStripButtonEllipse_Click(object sender, EventArgs e)
        {
            DrawEllipse();
        }

        private void toolStripButtonLine_Click(object sender, EventArgs e)
        {
            DrawLine();
        }

        private void toolStripButtonPencil_Click(object sender, EventArgs e)
        {
            DrawPolygon();
        }

        private void toolStripButtonAbout_Click(object sender, EventArgs e)
        {
            DisplayAbout();
        }

        private void toolStripButtonUndo_Click(object sender, EventArgs e)
        {
            Undo();
        }

        /// <summary>
        /// Provides key shortcut functionality.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Control | Keys.Z:
                    {
                        Undo();
                        return true;
                    }
                case Keys.Control | Keys.X:
                    {
                        Redo();
                        return true;
                    }
                case Keys.Control | Keys.S:
                    {
                        FileSave();
                        return true;
                    }
                case Keys.Delete:
                    {
                        CmdDelete command = new CmdDelete(drawSurface.GraphicsCollection);
                        if (drawSurface.GraphicsCollection.DeleteSelection())
                        {
                            drawSurface.Refresh();
                            drawSurface.AddCommandToHistory(command);
                        }
                        return true;
                    }
                case Keys.Control | Keys.A:
                    {
                        drawSurface.GraphicsCollection.SelectAll();
                        drawSurface.Refresh();
                        return true;
                    }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void toolStripButtonRedo_Click(object sender, EventArgs e)
        {
            Redo();
        }

        #endregion Toolbar Event Handlers

        #region Menu Event Handlers

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileNew();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileOpen();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileSave();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileSaveAs();
        }

        private void exportToJpgToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToJPG();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            drawSurface.GraphicsCollection.SelectAll();
            drawSurface.Refresh();

        }

        private void unselectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            drawSurface.GraphicsCollection.UnselectAll();
            drawSurface.Refresh();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CmdDelete command = new CmdDelete(drawSurface.GraphicsCollection);

            if (drawSurface.GraphicsCollection.DeleteSelection())
            {
                drawSurface.Refresh();
                drawSurface.AddCommandToHistory(command);
            }
        }

        private void deleteAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CmdDeleteAll command = new CmdDeleteAll(drawSurface.GraphicsCollection);

            if (drawSurface.GraphicsCollection.Clear())
            {
                drawSurface.Refresh();
                drawSurface.AddCommandToHistory(command);
            }
        }

        private void moveToFrontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (drawSurface.GraphicsCollection.MoveSelectionToFront())
            {
                drawSurface.Refresh();
            }

        }

        private void moveToBackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (drawSurface.GraphicsCollection.MoveSelectionToBack())
            {
                drawSurface.Refresh();
            }
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (drawSurface.GraphicsCollection.ShowPropertiesDialog(drawSurface))
            {
                drawSurface.Refresh();
            }

        }

        private void pointerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawPointer();
        }

        private void rectangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawRectangle();
        }

        private void ellipseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawEllipse();
        }

        private void lineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawLine();
        }

        private void pencilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawPolygon();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisplayAbout();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Redo();
        }

        #endregion Menu Event Handlers


        #region Event Handlers

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Create draw area
            drawSurface.Location = new System.Drawing.Point(0, 0);
            drawSurface.Size = new System.Drawing.Size(10, 10);
            drawSurface.Owner = null; // this;
            //this.Controls.Add(drawArea);


            drawSurface.Initialize(this);
            ResizeDrawingSurface();

            // Submit to Idle event to set controls state at idle time
            Application.Idle += delegate(object o, EventArgs a)
            {
                SetStateOfControls();
            };

            // Subscribe to DropDownOpened event for each popup menu
            // (see details in MainForm_DropDownOpened)
            foreach (ToolStripItem item in menuStrip.Items)
            {
                if (item.GetType() == typeof(ToolStripMenuItem))
                {
                    ((ToolStripMenuItem)item).DropDownOpened += MainForm_DropDownOpened;
                }
            }

        }

        /// <summary>
        /// Resize draw area when form is resized
        /// This is very handy.
        /// </summary>
        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Minimized && drawSurface != null)
            {
                ResizeDrawingSurface();
            }
        }

        /// <summary>
        /// TODO: Cleanup maybe.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        /// <summary>
        /// Popup menu item (File, Edit ...) is opened.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainForm_DropDownOpened(object sender, EventArgs e)
        {
            // Reset active tool to pointer.
            // This prevents bug in rare case when non-pointer tool is active, user opens
            // main main menu and after this clicks in the drawArea. MouseDown event is not
            // raised in this case (why ??), and MouseMove event works incorrectly.
            drawSurface.ActiveTool = DrawSurface.ToolType.Pointer;
        }
        #endregion Event Handlers

        #region Other Functions
        /// <summary>
        /// Set the state of  thje controls.
        /// Function is called at idle time.
        /// </summary>
        public void SetStateOfControls()
        {
            // Select active tool
            toolStripButtonPointer.Checked = (drawSurface.ActiveTool == DrawSurface.ToolType.Pointer);
            toolStripButtonRectangle.Checked = (drawSurface.ActiveTool == DrawSurface.ToolType.Rectangle);
            toolStripButtonEllipse.Checked = (drawSurface.ActiveTool == DrawSurface.ToolType.Ellipse);
            toolStripButtonLine.Checked = (drawSurface.ActiveTool == DrawSurface.ToolType.Line);
            toolStripButtonPencil.Checked = (drawSurface.ActiveTool == DrawSurface.ToolType.Polygon);

            bool objects = drawSurface.GraphicsCollection != null && (drawSurface.GraphicsCollection.Count > 0);
            bool selectedObjects = drawSurface.GraphicsCollection != null && (drawSurface.GraphicsCollection.SelectionCount > 0);

            // File operations
            //saveToolStripMenuItem.Enabled = objects;
            toolStripButtonSave.Enabled = objects;
            //saveAsToolStripMenuItem.Enabled = objects;

            // Edit operations
            deleteToolStripMenuItem.Enabled = selectedObjects;
            deleteAllToolStripMenuItem.Enabled = objects;
            selectAllToolStripMenuItem.Enabled = objects;
            unselectAllToolStripMenuItem.Enabled = objects;
            moveToFrontToolStripMenuItem.Enabled = selectedObjects;
            moveToBackToolStripMenuItem.Enabled = selectedObjects;
            propertiesToolStripMenuItem.Enabled = selectedObjects;

            // Undo, Redo
            undoToolStripMenuItem.Enabled = drawSurface.CanUndo;
            toolStripButtonUndo.Enabled = drawSurface.CanUndo;

            redoToolStripMenuItem.Enabled = drawSurface.CanRedo;
            toolStripButtonRedo.Enabled = drawSurface.CanRedo;
        }

        /// <summary>
        /// Set draw area to all form client space except toolbar
        /// </summary>
        private void ResizeDrawingSurface()
        {
            Rectangle rectangle = this.ClientRectangle;

            drawSurface.Left = rectangle.Left;
            drawSurface.Top = rectangle.Top + menuStrip.Height + toolStrip.Height;
            drawSurface.Width = rectangle.Width;
            drawSurface.Height = rectangle.Height - menuStrip.Height - toolStrip.Height;
        }

        /// <summary>
        /// Set Pointer draw tool
        /// </summary>
        private void DrawPointer()
        {
            drawSurface.ActiveTool = DrawSurface.ToolType.Pointer;
        }

        /// <summary>
        /// Set Rectangle draw tool
        /// </summary>
        private void DrawRectangle()
        {
            drawSurface.ActiveTool = DrawSurface.ToolType.Rectangle;
        }

        /// <summary>
        /// Set Ellipse draw tool
        /// </summary>
        private void DrawEllipse()
        {
            drawSurface.ActiveTool = DrawSurface.ToolType.Ellipse;
        }

        /// <summary>
        /// Set Line draw tool
        /// </summary>
        private void DrawLine()
        {
            drawSurface.ActiveTool = DrawSurface.ToolType.Line;
        }

        /// <summary>
        /// Set Polygon draw tool
        /// </summary>
        private void DrawPolygon()
        {
            drawSurface.ActiveTool = DrawSurface.ToolType.Polygon;
        }

        /// <summary>
        /// Set Triangle draw tool
        /// </summary>
        private void DrawTriangle()
        {
            drawSurface.ActiveTool = DrawSurface.ToolType.Triangle;
        }

        /// <summary>
        /// Show About dialog
        /// </summary>
        private void DisplayAbout()
        {
            AboutForm frm = new AboutForm();
            frm.ShowDialog(this);
        }

        /// <summary>
        /// Open new file
        /// </summary>
        private void FileNew()
        {
            drawSurface.GraphicsCollection.Clear();
            drawSurface.Refresh();

        }



        /// <summary>
        /// Open file
        /// </summary>
        private void FileOpen()
        {

        }

        /// <summary>
        /// Save file
        /// </summary>
        private void FileSave()
        {

        }

        /// <summary>
        /// Save As
        /// </summary>
        private void FileSaveAs()
        {

        }

        /// <summary>
        /// Export current graph document to jpg.
        /// </summary>
        private void ToJPG()
        {

        }

        /// <summary>
        /// Undo
        /// </summary>
        private void Undo()
        {
            drawSurface.Undo();
        }

        /// <summary>
        /// Redo
        /// </summary>
        private void Redo()
        {
            drawSurface.Redo();
        }

        #endregion

        #region DeadCode
        public void SetStatusStrip(string status)
        {
            Console.WriteLine(status);
            this.toolStripStatusLabel.Text = status;
            this.toolStripStatusLabel.Invalidate();
        }

        private void toolStripStatusLabel_Click(object sender, EventArgs e)
        {

        }

        private void drawArea_Load(object sender, EventArgs e)
        {

        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        #endregion

        private void fillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (drawSurface.GraphicsCollection.ShowFillDialog(drawSurface))
            //{
            //    drawSurface.Refresh();
            //}
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            using (var cdlg = new System.Windows.Forms.ColorDialog())
            {
                if (cdlg.ShowDialog() == DialogResult.OK)
                {
                    drawSurface.ActiveColor = cdlg.Color;
                    drawSurface.ActiveTool = DrawSurface.ToolType.Bucket;
                }
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            DrawTriangle();
        }




    }
}