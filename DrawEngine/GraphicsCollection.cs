#region Using directives

using System;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Drawing;
using System.Security.Permissions;
using System.Globalization;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Reflection;


#endregion

namespace DrawEngine
{
    public interface IDocumentDirtyObserver
    {
        void IsDirty(GraphicsCollection gList);
    }

    /// <summary>
    /// Models a collection of graphic objects.
    /// </summary>
    [Serializable]
    public class GraphicsCollection : ISerializable, IEnumerable<DrawShapeBase>
    {
        #region Members

        private List<DrawShapeBase> m_graphicsList;
        private List<IDocumentDirtyObserver> m_observers;
        private bool m_dirty;

        private readonly string entryCount = "Count";
        private readonly string entryType = "Type";

        #endregion


        #region Constructor
        public GraphicsCollection()
        {
            m_graphicsList = new List<DrawShapeBase>();
            m_observers = new List<IDocumentDirtyObserver>();
            m_dirty = false;
        }

        #endregion

        #region Serialization Support

        public void AddObserver(IDocumentDirtyObserver obs)
        {
            this.m_observers.Add(obs);
            NotifyDirty();
        }
        
        protected GraphicsCollection(SerializationInfo info, StreamingContext context)
        {
            this.m_graphicsList = new List<DrawShapeBase>();
            this.m_observers = new List<IDocumentDirtyObserver>();

            int n = info.GetInt32(entryCount);
            string typeName;
            DrawShapeBase drawObject;

            for (int i = 0; i < n; i++)
            {
                typeName = info.GetString(
                    String.Format(CultureInfo.InvariantCulture,
                        "{0}{1}",
                    entryType, i));

                drawObject = (DrawShapeBase)Assembly.GetExecutingAssembly().CreateInstance(
                    typeName);

                drawObject.LoadFromStream(info, i);

                m_graphicsList.Add(drawObject);
            }

        }

        /// <summary>
        /// Save object to serialization stream
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(entryCount, m_graphicsList.Count);

            int i = 0;

            foreach (DrawShapeBase o in m_graphicsList)
            {
                info.AddValue(
                    String.Format(CultureInfo.InvariantCulture,
                        "{0}{1}",
                        entryType, i),
                    o.GetType().FullName);

                o.SaveToStream(info, i);

                i++;
            }
        }

        #endregion

        #region Other functions

        protected void NotifyDirty()
        {
            if (this.m_dirty)
            {
                foreach (IDocumentDirtyObserver obs in m_observers)
                {
                    obs.IsDirty(this);
                }
            }
        }
        
        public void Draw(Graphics g)
        {
            int n = m_graphicsList.Count;
            DrawShapeBase o;

            //Reverse order to get the top object ( in the Z axis)
            for (int i = n - 1; i >= 0; i--)
            {
                o = m_graphicsList[i];

                o.Draw(g);

                if (o.Selected == true)
                {
                    o.DrawTracker(g);
                }
            }
        }


        /// <summary>
        /// For debugging purposes
        /// </summary>
        public void Dump()
        {
            Trace.WriteLine("");

            foreach ( DrawShapeBase o in m_graphicsList )
            {
                o.Dump();
            }
        }

        /// <summary>
        /// Clear all objects in the list
        /// </summary>
        /// <returns>
        /// true if at least one object is deleted
        /// </returns>
        public bool Clear()
        {
            bool result = (m_graphicsList.Count > 0);
            m_graphicsList.Clear();
            return result;
        }

        /// <summary>
        /// Tells if the document is dirty.
        /// </summary>
        public bool Dirty
        {
            get
            {
                return this.m_dirty;
            }
            set
            {
                this.m_dirty = value;
                if (this.m_dirty)
                {
                    NotifyDirty();
                }
            }
        }

        /// <summary>
        /// Count and this [nIndex] allow to read all graphics objects
        /// from GraphicsList in the loop.
        /// </summary>
        public int Count
        {
            get
            {
                return m_graphicsList.Count;
            }
        }

        public DrawShapeBase this[int index]
        {
            get
            {
                if (index < 0 || index >= m_graphicsList.Count)
                    return null;

                return m_graphicsList[index];
            }
        }
        /// <summary>
        /// Generetes the size to be used for drawing.
        /// </summary>
        /// <returns>The size computed from all the stored draw objects.</returns>
        public Size GetSize()
        {
            Size size = new Size(0, 0);
            foreach (DrawShapeBase obj in m_graphicsList)
            {
                Rectangle objRect = obj.GetBoundingBox();
                size.Width = Math.Max(size.Width, objRect.X + objRect.Width);
                size.Height = Math.Max(size.Height, objRect.Y + objRect.Height);
            }
            size.Width += 20;
            size.Height += 20;

            return size;
        }

        /// <summary>
        /// SelectedCount and GetSelectedObject allow to read
        /// selected objects in the loop
        /// </summary>
        public int SelectionCount
        {
            get
            {
                int n = 0;

                foreach (DrawShapeBase o in Selection)
                {
                    n++;
                }

                return n;
            }
        }


        /// <summary>
        /// Returns INumerable object which may be used for enumeration
        /// of selected objects.
        /// <remarks>
        /// Returning IEnumerable<DrawObject> breaks CLS-compliance
        /// (assembly CLSCompliant = true is removed from AssemblyInfo.cs).
        /// To make this program CLS-compliant, replace 
        /// IEnumerable<DrawObject> with IEnumerable. This requires
        /// casting to object at runtime.
        /// </remarks>
        /// </summary>
        /// <value></value>
        public IEnumerable<DrawShapeBase> Selection
        {
            get
            {
                foreach (DrawShapeBase o in m_graphicsList)
                {
                    if (o.Selected)
                    {
                        yield return o;
                    }
                }
            }
        }

        public void Add(DrawShapeBase obj)
        {
            // insert to the top of z-order
            m_graphicsList.Insert(0, obj);
            this.Dirty = true;
        }

        /// <summary>
        /// Insert object to specified place.
        /// Used for Undo.
        /// </summary>
        public void Insert(int index, DrawShapeBase obj)
        {
            if ( index >= 0  && index < m_graphicsList.Count )
            {
                m_graphicsList.Insert(index, obj);
            }
        }

        /// <summary>
        /// Replace object in specified place.
        /// Used for Undo.
        /// </summary>
        public void Replace(int index, DrawShapeBase obj)
        {
            if (index >= 0 && index < m_graphicsList.Count)
            {
                m_graphicsList.RemoveAt(index);
                m_graphicsList.Insert(index, obj);
            }
        }

        /// <summary>
        /// Remove object by index.
        /// Used for Undo.
        /// </summary>
        public void RemoveAt(int index)
        {
            m_graphicsList.RemoveAt(index);
        }

        /// <summary>
        /// Delete last added object from the list
        /// (used for Undo operation).
        /// </summary>
        public void DeleteLastAddedObject()
        {
            if ( m_graphicsList.Count > 0 )
            {
                m_graphicsList.RemoveAt(0);
            }
        }

        public void SelectInRectangle(Rectangle rectangle)
        {
            UnselectAll();

            foreach (DrawShapeBase o in m_graphicsList)
            {
                if (o.IntersectsWith(rectangle))
                    o.Selected = true;
            }

        }

        public void UnselectAll()
        {
            foreach (DrawShapeBase o in m_graphicsList)
            {
                o.Selected = false;
            }
        }

        public void SelectAll()
        {
            foreach (DrawShapeBase o in m_graphicsList)
            {
                o.Selected = true;
            }
        }

        /// <summary>
        /// Delete selected items
        /// </summary>
        /// <returns>
        /// true if at least one object is deleted
        /// </returns>
        public bool DeleteSelection()
        {
            bool result = false;

            int n = m_graphicsList.Count;

            for (int i = n - 1; i >= 0; i--)
            {
                if (((DrawShapeBase)m_graphicsList[i]).Selected)
                {
                    m_graphicsList.RemoveAt(i);
                    result = true;
                }
            }

            return result;
        }


        /// <summary>
        /// Move selected items to front (beginning of the list)
        /// </summary>
        /// <returns>
        /// true if at least one object is moved
        /// </returns>
        public bool MoveSelectionToFront()
        {
            int n;
            int i;
            List<DrawShapeBase> tempList;

            tempList = new List<DrawShapeBase>();
            n = m_graphicsList.Count;

            // Read source list in reverse order, add every selected item
            // to temporary list and remove it from source list
            for (i = n - 1; i >= 0; i--)
            {
                if ((m_graphicsList[i]).Selected)
                {
                    tempList.Add(m_graphicsList[i]);
                    m_graphicsList.RemoveAt(i);
                }
            }

            // Read temporary list in direct order and insert every item
            // to the beginning of the source list
            n = tempList.Count;

            for (i = 0; i < n; i++)
            {
                m_graphicsList.Insert(0, tempList[i]);
            }

            return (n > 0);
        }

        /// <summary>
        /// Move selected items to the end of the list.
        /// </summary>
        /// <returns>
        /// Returns true if at least one object was moved.
        /// </returns>
        public bool MoveSelectionToBack()
        {
            int n, i;
            List<DrawShapeBase> temp;

            temp = new List<DrawShapeBase>();
            n = m_graphicsList.Count;

            // Read the initial list in reverse then add every selected item
            // to a temporary list and remove it from the initial.
            for (i = n - 1; i >= 0; i--)
            {
                if ((m_graphicsList[i]).Selected)
                {
                    temp.Add(m_graphicsList[i]);
                    m_graphicsList.RemoveAt(i);
                }
            }

            // Read temporary list in reverse order and add every item
            // to the end of the source list
            n = temp.Count;
            for (i = n - 1; i >= 0; i--)
            {
                m_graphicsList.Add(temp[i]);
            }

            return (n > 0);
        }

        /// <summary>
        /// Get properties from selected objects and fill GraphicsProperties instance
        /// </summary>
        /// <returns></returns>
        private GraphicsProperties GetProperties()
        {
            GraphicsProperties properties = new GraphicsProperties();

            bool bFirst = true;

            int firstColor = 0;
            int firstPenWidth = 1;

            bool allColorsAreEqual = true;
            bool allWidthAreEqual = true;

            foreach (DrawShapeBase o in Selection)
            {
                if (bFirst)
                {
                    firstColor = o.Color.ToArgb();
                    firstPenWidth = o.PenWidth;
                    bFirst = false;
                }
                else
                {
                    if (o.Color.ToArgb() != firstColor)
                        allColorsAreEqual = false;

                    if (o.PenWidth != firstPenWidth)
                        allWidthAreEqual = false;
                }
            }


            if (allColorsAreEqual)
            {
                properties.Color = Color.FromArgb(firstColor);
            }

            if (allWidthAreEqual)
            {
                properties.PenWidth = firstPenWidth;
            }

            return properties;
        }

        /// <summary>
        /// Apply the give properties.
        /// </summary>
        /// <param name="properties">The proprties to apply.</param>
        /// <returns>True if at least one property is changed.</returns>
        private bool ApplyProperties(GraphicsProperties properties)
        {
            bool changed = false;

            foreach (DrawShapeBase o in m_graphicsList)
            {
                if (o.Selected)
                {
                    if (properties.Color.HasValue)
                    {
                        if (o.Color != properties.Color.Value)
                        {
                            o.Color = properties.Color.Value;
                            DrawShapeBase.LastUsedColor = properties.Color.Value;
                            changed = true;
                        }
                    }

                    if (properties.PenWidth.HasValue)
                    {
                        if (o.PenWidth != properties.PenWidth.Value)
                        {
                            o.PenWidth = properties.PenWidth.Value;
                            DrawShapeBase.LastUsedPenWidth = properties.PenWidth.Value;
                            changed = true;
                        }
                    }
                }
            }
            return changed;
        }

        /// <summary>
        /// Show Properties dialog. Return true if list is changed
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public bool ShowPropertiesDialog(DrawSurface parent)
        {
            if (SelectionCount < 1)
                return false;

            GraphicsProperties properties = GetProperties();
            PropertiesDialog dlg = new PropertiesDialog();
            dlg.Properties = properties;

            CmdChange c = new CmdChange(this);

            if (dlg.ShowDialog(parent) != DialogResult.OK)
                return false;

            if ( ApplyProperties(properties) )
            {
                c.NewState(this);
                parent.AddCommandToHistory(c);
            }

            return true;
        }

        public bool FillSelection(Color color, Point p, Graphics g)
        {
           try
            {
                foreach (var selected in Selection)
                {
                    selected.Fill(color, p, g);
                }
                return true;
            } catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return false;
            } 
        }
        public bool ShowFillDialog(DrawSurface parent)
        {
            if (SelectionCount != 1)
                return false;

           
            FillForm dlg = new FillForm();
            CmdChange c = new CmdChange(this);

            if (dlg.ShowDialog(parent) != DialogResult.OK)
                return false;

            if (FillSelection(dlg.Color, new Point(), null) == true)
            {
                c.NewState(this);
                parent.AddCommandToHistory(c);
            }
            return true;
        }

        #endregion

        #region IEnumerable

        public IEnumerator<DrawShapeBase> GetEnumerator()
        {
            return this.m_graphicsList.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
