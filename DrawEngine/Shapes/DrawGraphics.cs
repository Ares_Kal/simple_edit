﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DrawEngine.Shapes
{
    public class DrawGraphics
    {
        Graphics m_graphics;

        public List<Point> Painted { get; set; }
        public DrawGraphics(Graphics g)
        {
            m_graphics = g;
            m_graphics.SmoothingMode = this.SmoothingMode;
            Painted = new List<Point>();
        }
        public static DrawGraphics FromGDIGraphics(Graphics g)
        {
            return new DrawGraphics(g);
        }

        /// <summary>
        /// Draws a point specified by a coordinate pair.
        /// </summary>
        /// <param name="pen"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void DrawPoint(Pen pen, int x, int y)
        {
            m_graphics.DrawRectangle(pen, x, y, 1, 1);
            TryAdd(new Point(x, y));
        }

        public void FillPoint(Pen pen, int x, int y)
        {
            m_graphics.DrawRectangle(pen, x, y, 1, 1);
        }
        public System.Drawing.Drawing2D.SmoothingMode SmoothingMode
        {
            get;
            set;
        }

        /// <summary>
        /// Used to swap two objects.
        /// Not an atomic operation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        private void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp; temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        /// <summary>
        /// Lazy-compute every point that belongs to the line between the given coordinates.
        /// </summary>
        /// <param name="x0">The x-axis coordinate of the first point.</param>
        /// <param name="y0">The y-axis coordinate of the first point.</param>
        /// <param name="x1">The x-axis coordinate of the second point.</param>
        /// <param name="y1">The y-axis coordinate of the second point.</param>
        /// <exception cref="NullReferenceException"></exception>
        /// <returns>An IEnumerable that holds the given points.</returns>
        private IEnumerable<Point> BresehamPoints(int x0, int y0, int x1, int y1)
        {
            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
            if (steep)
            {
                Swap(ref x0, ref y0);
                Swap(ref x1, ref y1);
            }
            if (x0 > x1)
            {
                Swap(ref x0, ref x1);
                Swap(ref y0, ref y1);
            }
            int dx = x1 - x0;
            int dy = Math.Abs(y1 - y0);
            int error = dx / 2;
            int ystep = (y0 < y1) ? 1 : -1;
            int y = y0;
            for (int x = x0; x <= x1; x++)
            {
                yield return new Point((steep ? y : x), (steep ? x : y));
                error = error - dy;
                if (error < 0)
                {
                    y += ystep;
                    error += dx;
                }
            }
            yield break;
        }

        /// <summary>
        /// Draws a line specified by the coordinates of the points.
        /// </summary>
        /// <param name="x0">The x-axis coordinate of the first point.</param>
        /// <param name="y0">The y-axis coordinate of the first point.</param>
        /// <param name="x1">The x-axis coordinate of the second point.</param>
        /// <param name="y1">The y-axis coordinate of the second point.</param>
        public void DrawLine(Pen pen, int x0, int y0, int x1, int y1)
        {
            foreach (var point in BresehamPoints(x0, y0, x1, y1))
            {
                DrawPoint(pen, point.X, point.Y);
            }

        }

        public void DrawRectangle(Pen pen, Rectangle rectangle)
        {
            int width = rectangle.Width;
            int height = rectangle.Height;


            //ulc__________________urc
            //  |                  |     ulc = Upper Left Corner
            //  |                  |     ulc = Upper Right Corner
            //  |                  |     blc = Bottom Left Corner
            //  |__________________|     brc = Bottom Right Corner
            //blc                 brc

            Point ulc = new Point(rectangle.X, rectangle.Y);
            Point urc = new Point(rectangle.Right, rectangle.Y);
            Point blc = new Point(rectangle.Left, rectangle.Bottom);
            Point brc = new Point(rectangle.Right, rectangle.Bottom);

            //Draw the upper border
            DrawLine(pen, ulc.X, ulc.Y, urc.X, urc.Y);
            //Draw the left border
            DrawLine(pen, ulc.X, ulc.Y, blc.X, blc.Y);
            //Draw the right border
            DrawLine(pen, urc.X, urc.Y, brc.X, brc.Y);
            //Draw the bottom border
            DrawLine(pen, blc.X, blc.Y, brc.X, brc.Y);
        }

        public void DrawEllipse(Pen pen, Rectangle rectangle)
        {
            var points = new List<Point>();

        }

        public void DrawEllipse(Pen pen, List<double> points, int height)
        {
            // precision
            const int POINTS_ON_CURVE = 1000;

            double[] ptind = new double[points.Count];
            double[] p = new double[POINTS_ON_CURVE];
            points.CopyTo(ptind, 0);

            DrawBezier2D(ptind, (POINTS_ON_CURVE) / 2, p);
            for (int i = 1; i != POINTS_ON_CURVE - 1; i += 2)
            {
                DrawPoint(pen, (int)p[i + 1], (int)p[i]);
            }
        }

        /// <summary>
        /// Computes the binomial coefficient.
        /// </summary>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        private long BinomialCoefficient(long n, long k)
        {
            //This is optimized, in the sense that it doesn't use a factorial primitive
            if (k > n - k) k = n - k;
            long result = 1;
            for (long i = 1; i <= k; ++i)
            {
                result *= n - k + i;
                result /= i;
            }
            return result;
        }

        /// <summary>
        /// Calculate Bernstein basis.
        /// </summary>
        /// <param name="n"></param>
        /// <param name="i"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        private double ComputeBernstein(int n, int i, double t)
        {
            double basis;
            double ti; // t^i
            double tni; // (1 - t)^i 

            /* Prevent problems with pow */

            if (t == 0.0 && i == 0)
                ti = 1.0;
            else
                ti = Math.Pow(t, i);

            if (n == i && t == 1.0)
                tni = 1.0;
            else
                tni = Math.Pow((1 - t), (n - i));

            //Get the basis
            basis = BinomialCoefficient((long)n, (long)i) * ti * tni;
            return basis;
        }

        public void DrawBezier2D(double[] b, int cpts, double[] p)
        {
            int npts = (b.Length) / 2;
            int icount, jcount;
            double step, t;

            // Calculate points on curve
            icount = 0;
            t = 0;
            step = (double)1.0 / (cpts - 1);

            for (int i1 = 0; i1 != cpts; i1++)
            {
                if ((1.0 - t) < 5e-6)
                    t = 1.0;

                jcount = 0;
                p[icount] = 0.0;
                p[icount + 1] = 0.0;
                for (int i = 0; i != npts; i++)
                {
                    double basis = ComputeBernstein(npts - 1, i, t);
                    p[icount] += basis * b[jcount];
                    p[icount + 1] += basis * b[jcount + 1];
                    jcount = jcount + 2;
                }

                icount += 2;
                t += step;
            }
        }

        public void FillRectangle(SolidBrush brush, Rectangle rectangle)
        {
            m_graphics.FillRectangle(brush, rectangle);
        }

        public void FillEllipse(SolidBrush brush, Rectangle rectangle)
        {
            m_graphics.FillEllipse(brush, rectangle);
        }

        void TryAdd(Point p)
        {
            if (Painted == null)
            {
                Painted = new List<Point>();
            }
            Painted.Add(p);
        }
    }
}
