using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DrawEngine.Tools;

namespace DrawEngine
{
    [System.Runtime.InteropServices.GuidAttribute("AD5F1DE8-2B45-4B37-A0E5-81D38B009DD2")]
    public partial class DrawSurface : UserControl, IDocumentDirtyObserver
    {
        public enum ToolType
        {
            Pointer,
            Rectangle,
            Ellipse,
            Line,
            Polygon,
            Triangle,
            Bucket,
            ToolsCount
        };


        #region Ctor

        public DrawSurface()
        {
            InitializeComponent();
            initialized = false;
        }
        #endregion Constructor

        #region Members
        // The list that holds the shapes
        private GraphicsCollection m_graphicsCollection;
        //The active drawing tool
        private ToolType activeTool;
        //Every available tool
        private ToolBase[] tools;
        //active color
        private Color m_activeColor;

        //Parent form info
        private MainForm m_parent;
        private ContextMenuStrip m_ContextMenu;
        private UndoRedoQueue m_URqueue;
        private ToolStripStatusLabel m_status;

        private bool initialized;
        #endregion

        #region Properties

        /// <summary>
        /// Reference to the parent form.
        /// </summary>
        public MainForm Owner
        {
            get
            {
                return m_parent;
            }
            set
            {
                m_parent = value;
            }
        }

        /// <summary>
        /// Active drawing tool.
        /// </summary>
        public ToolType ActiveTool
        {
            get
            {
                return activeTool;
            }
            set
            {
                activeTool = value;
            }
        }

        /// <summary>
        /// List of graphics objects.
        /// </summary>
        public GraphicsCollection GraphicsCollection
        {
            get
            {
                return m_graphicsCollection;
            }
            set
            {
                m_graphicsCollection = value;
                if (this.GraphicsCollection != null)
                {
                    this.AdjustRendering();
                    this.m_graphicsCollection.AddObserver(this);
                }
            }
        }

        /// <summary>
        /// Return True if Undo operation is possible
        /// </summary>
        public bool CanUndo
        {
            get
            {
                if (m_URqueue != null)
                {
                    return m_URqueue.CanUndo;
                }

                return false;
            }
        }

        /// <summary>
        /// Return True if Redo operation is possible
        /// </summary>
        public bool CanRedo
        {
            get
            {
                if (m_URqueue != null)
                {
                    return m_URqueue.CanRedo;
                }

                return false;
            }
        }

        public ToolStripStatusLabel StatusLabel
        {
            get
            {
                return m_status;
            }
            set
            {
                m_status = value;
            }
        }

        public Color ActiveColor
        {
            get
            {
                return m_activeColor;
            }
            set
            {
                m_activeColor = value;
            }
        }
        #endregion

        #region Other Functions

        /// <summary>
        /// Initialization
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="docManager"></param>
        public void Initialize(MainForm owner)
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);

            // Keep reference to owner form
            Owner = owner;

            // set default tool
            activeTool = ToolType.Pointer;

            // create list of graphic objects
            GraphicsCollection = new GraphicsCollection();

            // Create undo manager
            m_URqueue = new UndoRedoQueue(GraphicsCollection);

            // create array of drawing tools
            tools = new ToolBase[(int)ToolType.ToolsCount];
            tools[(int)ToolType.Pointer] = new ToolPointer();
            tools[(int)ToolType.Rectangle] = new ToolRectangle();
            tools[(int)ToolType.Ellipse] = new ToolEllipse();
            tools[(int)ToolType.Line] = new ToolLine();
            tools[(int)ToolType.Polygon] = new ToolPolygon();
            tools[(int)ToolType.Bucket] = new ToolBucket();
            tools[(int)ToolType.Triangle] = new ToolTriangle();
            //AdjustRendering();
            initialized = true;
        }

        /// <summary>
        /// Add command to history.
        /// </summary>
        public void AddCommandToHistory(Command command)
        {
            m_URqueue.AddCommandToHistory(command);
        }

        /// <summary>
        /// Clear Undo history.
        /// </summary>
        public void ClearHistory()
        {
            m_URqueue.ClearHistory();
        }

        /// <summary>
        /// Undo
        /// </summary>
        public void Undo()
        {
            m_URqueue.Undo();
            Refresh();
        }

        /// <summary>
        /// Redo
        /// </summary>
        public void Redo()
        {
            m_URqueue.Redo();
            Refresh();
        }


        public void IsDirty(GraphicsCollection gList)
        {
            AdjustRendering();
        }


        /// <summary>
        /// Right-click handler
        /// </summary>
        /// <param name="e"></param>
        private void OnContextMenu(MouseEventArgs e)
        {
            // Change current selection if necessary

            Point point = new Point(Math.Abs(this.AutoScrollPosition.X) + e.X, Math.Abs(this.AutoScrollPosition.Y) + e.Y);

            int n = GraphicsCollection.Count;
            DrawShapeBase o = null;

            for (int i = 0; i < n; i++)
            {
                if (GraphicsCollection[i].HitTest(point) == 0)
                {
                    o = GraphicsCollection[i];
                    break;
                }
            }

            if (o != null)
            {
                if (!o.Selected)
                    GraphicsCollection.UnselectAll();

                // Select clicked object
                o.Selected = true;
            }
            else
            {
                GraphicsCollection.UnselectAll();
            }

            Refresh();      // in the case selection was changed

            // Show context menu.
            // Context menu items are filled from owner form Edit menu items.
            m_ContextMenu = new ContextMenuStrip();

            int nItems = m_parent.ContextParent.DropDownItems.Count;

            // Read Edit items and move them to context menu.
            // Since every move reduces number of items, read them in reverse order.
            // To get items in direct order, insert each of them to beginning.
            for (int i = nItems - 1; i >= 0; i--)
            {
                m_ContextMenu.Items.Insert(0, m_parent.ContextParent.DropDownItems[i]);
            }

            // Show context menu for owner form, so that it handles items selection.
            // Convert pointscroll from this window coordinates to owner's coordinates.
            point.X += this.Left;
            point.Y += this.Top;

            Point org = new Point(e.X, e.Y);
            m_ContextMenu.Show(m_parent, org);

            Owner.SetStateOfControls();  // enable/disable menu items

            // Context menu is shown, but owner's Edit menu is now empty.
            // Subscribe to context menu Closed event and restore items there.
            m_ContextMenu.Closed += (object sender, ToolStripDropDownClosedEventArgs args) =>
            {
                if (m_ContextMenu != null)      // precaution
                {
                    nItems = m_ContextMenu.Items.Count;

                    for (int k = nItems - 1; k >= 0; k--)
                    {
                        m_parent.ContextParent.DropDownItems.Insert(0, m_ContextMenu.Items[k]);
                    }
                }
            };
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Draw graphic objects and 
        /// group selection rectangle (optionally)
        /// </summary>
        private void DrawArea_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform((float)this.AutoScrollPosition.X, (float)this.AutoScrollPosition.Y);
            //Color.FromArgb(255, 255, 255)
            using (var brush = new SolidBrush(this.BackColor))
            {//Paint the area so we can draw on it.
                e.Graphics.FillRectangle(brush, this.ClientRectangle);
                if (m_graphicsCollection != null)
                {
                    m_graphicsCollection.Draw(e.Graphics);
                }
                else
                {

                }
            }
            e.Graphics.TranslateTransform(-(float)AutoScrollPosition.X, -(float)AutoScrollPosition.Y);
        }

        /// <summary>
        /// Mouse down.
        /// Left button down event is passed to active tool.
        /// Right button down event is handled in this class.
        /// </summary>
        private void DrawArea_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                tools[(int)activeTool].OnMouseDown(this, e);
            else if (e.Button == MouseButtons.Right)
                OnContextMenu(e);
        }

        /// <summary>
        /// Mouse move.
        /// Moving without button pressed or with left button pressed
        /// is passed to active tool.
        /// </summary>
        private void DrawArea_MouseMove(object sender, MouseEventArgs e)
        {
            if (!initialized)
                return;
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.None)
            {
                m_status.Text = e.Location.X + " , " + e.Location.Y;
                tools[(int)activeTool].OnMouseMove(this, e);
            }
            else
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Mouse up event.
        /// Left button up event is passed to active tool.
        /// </summary>
        private void DrawArea_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                tools[(int)activeTool].OnMouseUp(this, e);
        }

        #endregion Event Handlers

        void AdjustRendering()
        {
            Size size;

            if (GraphicsCollection != null)
            {
                size = GraphicsCollection.GetSize();
                AutoScrollMinSize = size;
            }
            else
            {
                AutoScrollMinSize = new Size(0, 0);
            }
            Invalidate();
        }
    }
}
