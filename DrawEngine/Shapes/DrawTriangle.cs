﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.InteropServices;
using Point = System.Drawing.Point;

namespace DrawEngine.Shapes
{
    class DrawTriangle : DrawPolygon
    {
        #region Members
        Point[] m_handles;
        DrawLine[] m_sides;
        List<Point> m_painted;
        #endregion

        #region Properties
        public override int HandleCount
        {
            get
            {
                return 3;
            }
        }
        #endregion

        #region Ctor
        public DrawTriangle()
        {
            m_handles = new Point[3];
            m_sides = new DrawLine[3];
            m_painted = new List<Point>();
        }
        public DrawTriangle(int x0, int y0, int x1, int y1, int x2, int y2): this()
        {
            m_handles[0] = new Point(x0, y0);
            m_handles[1] = new Point(x1, y1);
            m_handles[2] = new Point(x2, y2);

            m_sides[0] = new DrawLine(x0, y0, x1, y1);
            m_sides[1] = new DrawLine(x0, y0, x2, y2);
            m_sides[2] = new DrawLine(x1, y1, x2, y2);
        }
        public DrawTriangle(Point p1, Point p2, Point p3): this(p1.X, p1.Y, p2.X, p2.Y, p3.X, p3.Y)
        {
    
        }
        #endregion

        public override void Draw(Graphics g)
        {
            using (Pen pen = new Pen(Color, PenWidth))
            {
                var dg = Shapes.DrawGraphics.FromGDIGraphics(g);
                foreach (var side in m_sides)
                {
                    side.Draw(dg);
                }
                if (IsFilled)
                {
                    FillArea(pen, dg);
                }
                    
            }
           
        }

        /// <summary>
        /// Clone this instance
        /// </summary>
        public override DrawShapeBase Clone()
        {
            DrawTriangle drawTriangle = new DrawTriangle();

            drawTriangle.m_handles[0] = m_handles[0];
            drawTriangle.m_handles[1] = m_handles[1];
            drawTriangle.m_handles[2] = m_handles[2];

            FillDrawObjectFields(drawTriangle);
            return drawTriangle;
        }
        #region Member Functions
        public override Point GetHandle(int handleNumber)
        {
            return m_handles[handleNumber -1];
        }

        /// <summary>
        /// Calculates the median point for the shape.
        /// </summary>
        /// <returns></returns>
        public override Point GetMedian()
        {
            var point = new Point(0, 0);
            for (int i = 0; i < HandleCount; i++)
            {
                point.X += m_handles[i].X;
                point.Y += m_handles[i].Y;
            }
            point.X /= HandleCount;
            point.Y /= HandleCount;
            return point;
        }

        /// <summary>
        /// Move's the handle, specified by the handle name, to the provided position.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="handleNumber"></param>
        public override void MoveHandleTo(Point point, int handleNumber)
        {
            switch (handleNumber)
            {
                case 1:
                    m_handles[0] = point;
                    m_sides[0].MoveHandleTo(point, 1);
                    m_sides[1].MoveHandleTo(point, 1);
                    break;
                case 2:
                    m_handles[1] = point;
                    m_sides[0].MoveHandleTo(point, 2);
                    m_sides[2].MoveHandleTo(point, 1);
                    break;
                case 3:
                    m_handles[2] = point;
                    m_sides[1].MoveHandleTo(point, 2);
                    m_sides[2].MoveHandleTo(point, 2);
                    break;
            }
        }

        /// <summary>
        /// Tests if the hit point is within the shape.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public override int HitTest(Point point)
        {
            if (Selected)
            {
                for (int i = 1; i <= HandleCount; i++)
                {
                    if (GetHandleRectangle(i).Contains(point))
                        return i;
                }
            }

            if (IsWithinBounds(point))
                return 0;

            return -1;
        }

        /// <summary>
        /// Returns the bounding box of the shape.
        /// </summary>
        /// <returns></returns>
        public override Rectangle GetBoundingBox()
        {
            return this.GetNormalizedRectangle(m_handles[0], m_handles[1], m_handles[2]);
        }

        /// <summary>
        /// Tests if the given point is withing the shape.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public override bool IsWithinBounds(System.Drawing.Point p)
        {
            var A = m_handles[0];
            var B = m_handles[1];
            var C = m_handles[2];
            var P = p;

            var planeAB = (A.X - p.X) * (B.Y - p.Y) - (B.X - P.X) * (A.Y - P.Y);
            var planeBC = (B.X - P.X) * (C.Y - P.Y) - (C.X - P.X) * (B.Y - P.Y);
            var planeCA = (C.X - P.X) * (A.Y - P.Y) - (A.X - P.X) * (C.Y - P.Y);
            return Math.Sign(planeAB) == Math.Sign(planeBC) && Math.Sign(planeBC) == Math.Sign(planeCA);
        }

        public override void Move(int deltaX, int deltaY)
        {
            for (int i = 1; i <= HandleCount; i++)
            {
                var handle = GetHandle(i);
                var dp = new Point(handle.X + deltaX, handle.Y + deltaY);
                MoveHandleTo(dp, i);
            }

            foreach (var side in m_sides)
            {
                side.Move(deltaX, deltaY);
            }
        }

        #endregion

        #region Helpers
        /// <summary>
        /// Returns the smallest of three 32-bit integers.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        private int MinArity3(int x, int y, int z)
        {
            var minOfArity2 = Math.Min(x, y);
            return z < minOfArity2 ? z : minOfArity2;
        }

        /// <summary>
        /// Returns the greatest of three 32-bit integers.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        private int MaxArity3(int x, int y, int z)
        {
            var maxOfArity2 = Math.Max(x, y);
            return z > maxOfArity2 ? z : maxOfArity2;
        }
        private Point GetUpperLeftCorner(Point p0, Point p1, Point p2)
        {
            var x = MinArity3(p0.X, p1.X, p2.X);
            var y = MaxArity3(p0.Y, p1.Y, p2.Y);
            return new Point(x, y);
        }

        private int GetBoundingWidth(Point p0, Point p1, Point p2)
        {
            var diff = MaxArity3(p0.X, p1.X, p2.X) - MinArity3(p0.X, p1.X, p2.X);
            return diff;
        }

        private int GetBoundingHeight(Point p0, Point p1, Point p2)
        {
            var diff = MaxArity3(p0.Y, p1.Y, p2.Y) - MinArity3(p0.Y, p1.Y, p2.Y);
            return diff;
        }
        
        public Rectangle GetNormalizedRectangle(Point p0, Point p1, Point p2)
        {
            Rectangle r = new Rectangle();
            //Upper left corner.
            Point ulc = GetUpperLeftCorner(p0, p1, p2);
            r.X = ulc.X;
            r.Y = ulc.Y;

            r.Height = GetBoundingHeight(p0, p1, p2);
            r.Width = GetBoundingWidth(p0, p1, p2);
            return r;
        }
        #endregion
     
    }

}
