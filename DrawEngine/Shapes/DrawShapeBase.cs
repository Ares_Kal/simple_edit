using System;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace DrawEngine
{
	/// <summary>
	/// Base class for all draw objects
	/// </summary>
	public abstract class DrawShapeBase
	{
        #region Members

        //So that we can provide order-agnostic Undo/Redo functions.
        private int m_ObjID;   

        private bool m_selected;
        private Color m_color;
        private int m_penWidth;
        private Color m_areaColor;
        private bool m_filled;

        

        // Last used property values (may be kept in the Registry)
        private static Color lastUsedColor = Color.Black;
        private static int lastUsedPenWidth = 1;

        // Entry names for serialization
        private const string entryColor = "Color";
        private const string entryPenWidth = "PenWidth";

        #endregion

        public DrawShapeBase()
        {
            m_ObjID = this.GetHashCode();
        }

        #region Properties

        /// <summary>
        /// Selection flag
        /// </summary>
        public bool Selected
        {
            get
            {
                return m_selected;
            }
            set
            {
                m_selected = value;
            }
        }

        /// <summary>
        /// Color
        /// </summary>
        public Color Color
        {
            get
            {
                return m_color;
            }
            set
            {
                m_color = value;
            }
        }

        /// <summary>
        /// Pen width
        /// </summary>
        public int PenWidth
        {
            get
            {
                return m_penWidth;
            }
            set
            {
                m_penWidth = value;
            }
        }

        /// <summary>
        /// Number of handles
        /// </summary>
        public virtual int HandleCount
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Object ID
        /// </summary>
        public int ID
        {
            get { return m_ObjID; }
            set { m_ObjID = value; }
        }


        /// <summary>
        /// Last used color
        /// </summary>
        public static Color LastUsedColor
        {
            get
            {
                return lastUsedColor;
            }
            set
            {
                lastUsedColor = value;
            }
        }

        /// <summary>
        /// Last used pen width
        /// </summary>
        public static int LastUsedPenWidth
        {
            get
            {
                return lastUsedPenWidth;
            }
            set
            {
                lastUsedPenWidth = value;
            }
        }

        /// <summary>
        /// The color of the inside area.
        /// </summary>
        public Color AreaColor
        {
            get 
            { 
                return this.m_areaColor; 
            }
            set 
            { 
                this.m_areaColor = value; 
            }
        }

        /// <summary>
        /// Returns true if the inside area is colored.
        /// </summary>
        public bool IsFilled
        {
            get
            {
                return m_filled;
            }
            set
            {
                m_filled = value;
            }
        }
        #endregion

        #region Virtual Methods

        /// <summary>
        /// Clone this instance.
        /// </summary>
        public abstract DrawShapeBase Clone();

        /// <summary>
        /// Draw the object in the screen.
        /// </summary>
        /// <param name="g">A GDI+ graphics object.</param>
        public virtual void Draw(Graphics g)
        {
        }
        
        public abstract Rectangle GetBoundingBox();

        /// <summary>
        /// Get handle pointscroll by 1-based number
        /// </summary>
        /// <param name="handleNumber"></param>
        /// <returns></returns>
        public virtual Point GetHandle(int handleNumber)
        {
            return new Point(0, 0);
        }

        /// <summary>
        /// Get handle rectangle by 1-based number
        /// </summary>
        /// <param name="handleNumber"></param>
        /// <returns></returns>
        public virtual Rectangle GetHandleRectangle(int handleNumber)
        {
            Point point = GetHandle(handleNumber);

            return new Rectangle(point.X - 3, point.Y - 3, 7, 7);
        }

        /// <summary>
        /// Draw tracker for selected object
        /// </summary>
        /// <param name="g"></param>
        public virtual void DrawTracker(Graphics g)
        {
            if (Selected)
            {
                using (var brush = new SolidBrush(Color.Black))
                {
                    for (int i = 1; i <= HandleCount; i++)
                    {
                        g.FillRectangle(brush, GetHandleRectangle(i));
                    }

                    brush.Dispose();
                }

            }
        }

        public virtual void DrawTracker(Shapes.DrawGraphics g)
        {
            if (Selected)
            {
                using (var brush = new SolidBrush(Color.Black))
                {
                    for (int i = 1; i <= HandleCount; i++)
                    {
                        g.FillRectangle(brush, GetHandleRectangle(i));
                    }

                    brush.Dispose();
                }

            }
        }

        /// <summary>
        /// Hit test.
        /// Return value: -1 - no hit
        ///                0 - hit anywhere
        ///                > 1 - handle number
        /// </summary>
        /// <param name="pointscroll"></param>
        /// <returns></returns>
        public virtual int HitTest(Point point)
        {
            return -1;
        }


        /// <summary>
        /// Test whether the point is within the shape.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public virtual bool IsWithinBounds(Point point)
        {
            return false;
        }
        

        /// <summary>
        /// Get cursor for the handle
        /// </summary>
        /// <param name="handleNumber"></param>
        /// <returns></returns>
        public virtual Cursor GetHandleCursor(int handleNumber)
        {
            return Cursors.Default;
        }

        /// <summary>
        /// Test whether object intersects with rectangle
        /// </summary>
        /// <param name="rectangle"></param>
        /// <returns></returns>
        public virtual bool IntersectsWith(Rectangle rectangle)
        {
            return false;
        }

        /// <summary> 
        /// Move the object.
        /// </summary>
        /// <param name="deltaX"></param>
        /// <param name="deltaY"></param>
        public virtual void Move(int deltaX, int deltaY)
        {
        }

        /// <summary>
        /// Move handle to the pointscroll
        /// </summary>
        /// <param name="pointscroll"></param>
        /// <param name="handleNumber"></param>
        public virtual void MoveHandleTo(Point point, int handleNumber)
        {
        }

        /// <summary>
        /// Dump (for debugging)
        /// </summary>
        public virtual void Dump()
        {
            Trace.WriteLine(this.GetType().Name);
            Trace.WriteLine("Selected = " + 
                m_selected.ToString(CultureInfo.InvariantCulture)
                + " ID = " + m_ObjID.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Normalize object.
        /// Call this function in the end of object resizing.
        /// </summary>
        public virtual void Normalize()
        {
        }


        /// <summary>
        /// Save object to serialization stream
        /// </summary>
        /// <param name="info"></param>
        /// <param name="orderNumber"></param>
        public virtual void SaveToStream(SerializationInfo info, int orderNumber)
        {
            info.AddValue(
                String.Format(CultureInfo.InvariantCulture,
                    "{0}{1}",
                    entryColor, orderNumber),
                Color.ToArgb());

            info.AddValue(
                String.Format(CultureInfo.InvariantCulture,
                "{0}{1}",
                entryPenWidth, orderNumber),
                PenWidth);
        }

        /// <summary>
        /// Load object from serialization stream
        /// </summary>
        /// <param name="info"></param>
        /// <param name="orderNumber"></param>
        public virtual void LoadFromStream(SerializationInfo info, int orderNumber)
        {
            int n = info.GetInt32(
                String.Format(CultureInfo.InvariantCulture,
                    "{0}{1}",
                    entryColor, orderNumber));

            Color = Color.FromArgb(n);

            PenWidth = info.GetInt32(
                String.Format(CultureInfo.InvariantCulture,
                "{0}{1}",
                entryPenWidth, orderNumber));

            m_ObjID = this.GetHashCode();
        }

        #endregion

        #region Helper functions

        /// <summary>
        /// Initialization of members.
        /// </summary>
        protected void Initialize()
        {
            m_color = lastUsedColor;
            m_penWidth = LastUsedPenWidth;
            m_filled = false;
        }

        /// <summary>
        /// Copy fields from this instance to cloned instance drawObject.
        /// Called from Clone functions of derived classes.
        /// </summary>
        protected void FillDrawObjectFields(DrawShapeBase drawObject)
        {
            drawObject.m_selected = this.m_selected;
            drawObject.m_color = this.m_color;
            drawObject.m_penWidth = this.m_penWidth;
            drawObject.ID = this.ID;
        }

        #endregion

        public virtual Point GetMedian()
        {
            var point = new Point(0, 0);
            for (int i = 0; i < HandleCount; i++)
            {
                point.X += GetHandle(i).X;
                point.Y += GetHandle(i).Y;
            }
            point.X /= HandleCount;
            point.Y /= HandleCount;
            return point;
        }

        protected List<Point> AdjacentPixels(Point pixel)
        {
            var x = pixel.X;
            var y = pixel.Y;
            var ret = new List<Point>();
            ret.Add(new Point(x - 1, y - 1));
            ret.Add(new Point(x - 1, y));
            ret.Add(new Point(x - 1, y + 1));
            ret.Add(new Point(x, y - 1));
            ret.Add(new Point(x, y + 1));
            ret.Add(new Point(x + 1, y - 1));
            ret.Add(new Point(x + 1, y));
            ret.Add(new Point(x + 1, y + 1));
            return ret;
        }

        /// <summary>
        /// Fills the shape's area with the shape's AreaColor.<remarks>This is rediculously slow, even without recursion.</remarks>
        /// </summary>
        /// <param name="pen"></param>
        /// <param name="pixel"></param>
        /// <param name="g"></param>
        void FloodFill(Pen pen, Point pixel, Shapes.DrawGraphics g)
        {
            var queue = new Queue<Point>();
            var processed = new HashSet<Point>();
        
            queue.Enqueue(pixel);
            while (queue.Any())
            {
                var current = queue.Dequeue();
                if (g.Painted.Contains(current) || processed.Contains(current))
                    continue;

                g.FillPoint(pen, current.X, current.Y);
                processed.Add(current);
                foreach (var p in AdjacentPixels(current))
                {
                    TryEnqueue(queue, p, g);
                }
            }
        }

        private bool TryEnqueue(Queue<Point> queue, Point p, Shapes.DrawGraphics g)
        {
            if (p.X < 0 || p.Y < 0 || g.Painted.Contains(p) || !IsWithinBounds(p))
                return false;
            else
            {
                queue.Enqueue(p);
                return true;
            }
        }


        /// <summary>
        /// Paint the shape's area with the selected <see cref="DrawShapeBase.AreaColor"/> color.
        /// </summary>
        /// <param name="pen"></param>
        /// <param name="g"></param>
        protected void FillArea(Pen pen, Shapes.DrawGraphics g)
        {
            var mid = GetMedian();
            FloodFill(pen, mid, g);
        }
      
        public virtual void Fill(System.Drawing.Color color, Point p, Graphics g)
        {

        }    
    }
}
