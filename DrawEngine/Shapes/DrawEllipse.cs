using System;
using System.Windows.Forms;
using System.Drawing;
using DrawEngine;
using System.Collections.Generic;

namespace DrawEngine
{
	/// <summary>
	/// Graphic object that represents an ellipse.
	/// </summary>
	class DrawEllipse : DrawEngine.DrawRectangle
	{
		public DrawEllipse() : this(0, 0, 1, 1)
		{
		}

        public DrawEllipse(int x, int y, int width, int height) : base()
        {
            Rectangle = new Rectangle(x, y, width, height);
            Initialize();
        }

        /// <summary>
        /// Deep copy of the instance.
        /// </summary>
        public override DrawEngine.DrawShapeBase Clone()
        {
            DrawEllipse drawEllipse = new DrawEllipse();
            drawEllipse.Rectangle = this.Rectangle;

            FillDrawObjectFields(drawEllipse);
            return drawEllipse;
        }


        public override void Draw(Graphics g)
        {
            Pen pen = new Pen(Color, PenWidth);
            //g.DrawEllipse(pen, DrawRectangle.GetNormalizedRectangle(Rectangle));
            var upperQuandrant = new List<double>() {
                GetHandle(8).X, GetHandle(8).Y, 
                GetHandle(1).X, GetHandle(1).Y, 
                GetHandle(3).X, GetHandle(3).Y,
                GetHandle(4).X, GetHandle(4).Y
            };

            var lowerQuadrant = new List<double>(){
                GetHandle(8).X, GetHandle(8).Y,
                GetHandle(7).X, GetHandle(7).Y,
                GetHandle(5).X, GetHandle(5).Y,
                GetHandle(4).X, GetHandle(4).Y
            };
            var dg = DrawEngine.Shapes.DrawGraphics.FromGDIGraphics(g);
            dg.DrawEllipse(pen, upperQuandrant, Rectangle.Height);
            dg.DrawEllipse(pen, lowerQuadrant, Rectangle.Height);

            if (IsFilled)
                dg.FillEllipse(new SolidBrush(AreaColor), this.Rectangle);
            pen.Dispose();
        }


	}
}
