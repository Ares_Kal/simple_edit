using System;
using System.Collections.Generic;
using System.Text;

namespace DrawEngine
{
    /// <summary>
    /// Class that provides Undo/Redo functionality.
    /// <remarks>It is implemented using the Command pattern.</remarks>
    /// </summary>
    class UndoRedoQueue
    {
        #region Class Members

        GraphicsCollection m_graphics;

        List<Command> m_history;
        int m_nextUndo;

        #endregion  Class Members

        #region Constructor

        public UndoRedoQueue(GraphicsCollection graphicsList)
        {
            this.m_graphics = graphicsList;

            ClearHistory();
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Return true if Undo operation is available
        /// </summary>
        public bool CanUndo
        {
            get
            {
                // If the NextUndo pointer is -1, no commands to undo
                if (m_nextUndo < 0 ||
                    m_nextUndo > m_history.Count - 1)   // precaution
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Return true if Redo operation is available
        /// </summary>
        public bool CanRedo
        {
            get
            {
                // If the NextUndo pointer points to the last item, no commands to redo
                if (m_nextUndo == m_history.Count - 1)
                {
                    return false;
                }

                return true;
            }
        }

        #endregion Properties

        #region Public Functions

        /// <summary>
        /// Clear History
        /// </summary>
        public void ClearHistory()
        {
            m_history = new List<Command>();
            m_nextUndo = -1;
        }

        /// <summary>
        /// Add new command to history.
        /// Called by client after executing some action.
        /// </summary>
        /// <param name="command"></param>
        public void AddCommandToHistory(Command command)
        {
            // Purge history list
            this.TrimHistoryList();

            // Add command and increment undo counter
            m_history.Add(command);

            m_nextUndo++;
        }

        /// <summary>
        /// Undo
        /// </summary>
        public void Undo()
        {
            if ( ! CanUndo )
            {
                return;
            }

            // Get the Command object to be undone
            Command command = m_history[m_nextUndo];

            // Execute the Command object's undo method
            command.Undo(m_graphics);

            // Move the pointer up one item
            m_nextUndo--;
        }

        /// <summary>
        /// Redo
        /// </summary>
        public void Redo()
        {
            if ( ! CanRedo )
            {
                return;
            }

            // Get the Command object to redo
            int itemToRedo = m_nextUndo + 1;
            Command command = m_history[itemToRedo];

            // Execute the Command object
            command.Redo(m_graphics);

            // Move the undo pointer down one item
            m_nextUndo++;
        }

        #endregion Public Functions

        #region Private Functions

        private void TrimHistoryList()
        {
            // We can redo any undone command until we execute a new 
            // command. The new command takes us off in a new direction,
            // which means we can no longer redo previously undone actions. 
            // So, we purge all undone commands from the history list.*/

            // Exit if no items in History list
            if (m_history.Count == 0)
            {
                return;
            }

            // Exit if NextUndo points to last item on the list
            if (m_nextUndo == m_history.Count - 1)
            {
                return;
            }

            // Purge all items below the NextUndo pointer
            for (int i = m_history.Count - 1; i > m_nextUndo; i--)
            {
                m_history.RemoveAt(i);
            }
        }

        #endregion
    }
}
