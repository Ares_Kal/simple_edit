#region Using directives

using System;
using System.Windows.Forms;
using System.Drawing;

#endregion

namespace DrawEngine
{
    /// <summary>
    /// Contains info for a graphics onbject 
    /// </summary>
    class GraphicsProperties
    {
        private Color? m_color;
        private int? m_penWidth;

        public Color? Color
        {
            get
            {
                return m_color;
            }
            set
            {
                m_color = value;
            }
        }

        public int? PenWidth
        {
            get
            {
                return m_penWidth;
            }
            set
            {
                m_penWidth = value;
            }
        }
    }
}
