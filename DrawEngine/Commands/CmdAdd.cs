using System;
using System.Collections.Generic;
using System.Text;

namespace DrawEngine
{
    /// <summary>
    /// Add new object command
    /// </summary>
    class CmdAdd : Command
    {
        DrawShapeBase drawObject;

        // Create this command with DrawObject instance added to the list
        public CmdAdd(DrawShapeBase drawObject) : base()
        {
            // Keep copy of added object
            this.drawObject = drawObject.Clone();
        }

        public override void Undo(GraphicsCollection list)
        {
            list.DeleteLastAddedObject();
        }

        public override void Redo(GraphicsCollection list)
        {
            list.UnselectAll();
            list.Add(drawObject);
        }
    }
}
