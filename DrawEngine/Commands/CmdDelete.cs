using System;
using System.Collections.Generic;
using System.Text;

namespace DrawEngine
{
    /// <summary>
    /// Delete command
    /// </summary>
    class CmdDelete : Command
    {
        List<DrawShapeBase> cloneList;    // contains selected items which are deleted

        // Create this command BEFORE applying Delete All function.
        public CmdDelete(GraphicsCollection graphicsList)
        {
            cloneList = new List<DrawShapeBase>();

            // Make clone of the list selection.

            foreach(DrawShapeBase o in graphicsList.Selection)
            {
                cloneList.Add(o.Clone());
            }
        }

        public override void Undo(GraphicsCollection list)
        {
            list.UnselectAll();

            // Add all objects from cloneList to list.
            foreach(DrawShapeBase o in cloneList)
            {
                list.Add(o);
            }
        }

        public override void Redo(GraphicsCollection list)
        {
            // Delete from list all objects kept in cloneList
            
            int n = list.Count;

            for ( int i = n - 1; i >= 0; i-- )
            {
                bool toDelete = false;
                DrawShapeBase objectToDelete = list[i];

                foreach(DrawShapeBase o in cloneList)
                {
                    if ( objectToDelete.ID == o.ID )
                    {
                        toDelete = true;
                        break;
                    }
                }

                if ( toDelete )
                {
                    list.RemoveAt(i);
                }
            }
        }
    }
}
