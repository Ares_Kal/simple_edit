using System;
using System.Collections.Generic;
using System.Text;

namespace DrawEngine
{
    /// <summary>
    /// Changing state of existing objects:
    /// move, resize, change properties.
    /// </summary>
    class CmdChange : Command
    {
        // Selected object(s) before operation
        List<DrawShapeBase> listBefore;

        // Selected object(s) after operation
        List<DrawShapeBase> listAfter;
        

        // Create this command BEFORE operation.
        public CmdChange(GraphicsCollection graphicsList)
        {
            // Keep objects state before operation.
            FillList(graphicsList, ref listBefore);
        }

        // Call this function AFTER operation.
        public void NewState(GraphicsCollection graphicsList)
        {
            // Keep objects state after operation.
            FillList(graphicsList, ref listAfter);
        }

        public override void Undo(GraphicsCollection list)
        {
            // Replace all objects in the list with objects from listBefore
            ReplaceObjects(list, listBefore);
        }

        public override void Redo(GraphicsCollection list)
        {
            // Replace all objects in the list with objects from listAfter
            ReplaceObjects(list, listAfter);
        }

        // Replace objects in graphicsList with objects from list
        private void ReplaceObjects(GraphicsCollection graphicsList, List<DrawShapeBase> list)
        {
            for ( int i = 0; i < graphicsList.Count; i++ )
            {
                DrawShapeBase replacement = null;

                foreach(DrawShapeBase o in list)
                {
                    if ( o.ID == graphicsList[i].ID )
                    {
                        replacement = o;
                        break;
                    }
                }

                if ( replacement != null )
                {
                    graphicsList.Replace(i, replacement);
                }
            }
        }

        // Fill list from selection
        private void FillList(GraphicsCollection graphicsList, ref List<DrawShapeBase> listToFill)
        {
            listToFill = new List<DrawShapeBase>();

            foreach (DrawShapeBase o in graphicsList.Selection)
            {
                listToFill.Add(o.Clone());
            }
        }
    }
}
