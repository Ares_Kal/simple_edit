using System;
using System.Collections.Generic;
using System.Text;

namespace DrawEngine
{
    /// <summary>
    /// Delete All command
    /// </summary>
    class CmdDeleteAll : Command
    {
        List<DrawShapeBase> cloneList;

        // Create this command BEFORE applying Delete All function.
        public CmdDeleteAll(GraphicsCollection graphicsList)
        {
            cloneList = new List<DrawShapeBase>();

            // Make clone of the whole list.
            // Add objects in reverse order because GraphicsList.Add
            // insert every object to the beginning.
            int n = graphicsList.Count;

            for ( int i = n - 1; i >= 0; i-- )
            {
                cloneList.Add(graphicsList[i].Clone());
            }
        }

        public override void Undo(GraphicsCollection list)
        {
            // Add all objects from clone list to list -
            // opposite to DeleteAll
            foreach (DrawShapeBase o in cloneList)
            {
                list.Add(o);
            }
        }

        public override void Redo(GraphicsCollection list)
        {
            // Clear list - make DeleteAll again
            list.Clear();
        }
    }
}
